wine_get_version.py [![Unlicensed work](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/)
===================
~~[wheel (GHA via `nightly.link`)](https://nightly.link/KOLANICH-libs/wine_get_version.py/workflows/CI/master/wine_get_version-0.CI-py3-none-any.whl)~~
~~[![GitHub Actions](https://github.com/KOLANICH-libs/wine_get_version.py/workflows/CI/badge.svg)](https://github.com/KOLANICH-libs/wine_get_version.py/actions/)~~
![N∅ dependencies](https://shields.io/badge/-N∅_deps!-0F0)
[![Libraries.io Status](https://img.shields.io/librariesio/github/KOLANICH-libs/wine_get_version.py.svg)](https://libraries.io/github/KOLANICH-libs/wine_get_version.py)
[![Code style: antiflash](https://img.shields.io/badge/code%20style-antiflash-FFF.svg)](https://codeberg.org/KOLANICH-tools/antiflash.py)

Just a small wrapper around `wine_get_version()`.
